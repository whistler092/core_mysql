using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace base_mysql.Models.ManageViewModels
{
    public class FactorViewModel
    {
        public string Purpose { get; set; }
    }
}
