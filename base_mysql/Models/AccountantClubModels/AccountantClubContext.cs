using base_mysql.Models;
using Microsoft.EntityFrameworkCore;

namespace base_mysql.Data
{
    public class AccountantClubContext : DbContext
    {
        public AccountantClubContext(DbContextOptions<AccountantClubContext> options)
            : base(options)
        {
        }

        public DbSet<Blog> Blogs { get; set; }
        public DbSet<Post> Posts { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<Blog>().ToTable("Blog");
            builder.Entity<Post>().ToTable("Post");

            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);
        }

    }
}