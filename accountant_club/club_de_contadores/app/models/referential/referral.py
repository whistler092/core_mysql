# -*- coding: utf-8 -*-

from datetime import datetime
from ... import Base
from sqlalchemy import String, Integer, Column, DateTime, and_, or_,ForeignKey
from ...exceptions import ValidationError, IntegrityError, InternalServerError
from flask import jsonify, g
from ... import session
from sqlalchemy.exc import IntegrityError as sqlIntegrityError
from sqlalchemy.orm import relationship, backref


class Referral(Base):

    __tablename__ = 'referrals'

    referral_id = Column(Integer, primary_key=True)
    create_time = Column(DateTime, default=datetime.now())
    update_time = Column(DateTime, default=datetime.now())
    company = Column(String(255))
    contact_name = Column(String(255))
    phone = Column(String(45))
    cellphone = Column(String(45))
    email = Column(String(45))
    city = Column(String(45))
    department = Column(String(45))
    current_softpymes = Column(String(45))
    status = Column(String(45))

    profile_id = Column(Integer, ForeignKey('Profile.profile_id'))
    #profile = relationship('Profile', foreign_keys=[profile_id])

    def export_data(self):
        """
        Return data by a Referral
        :return Referrals object in JSON format
        """
        return {
            'referral_id': self.referral_id,
            'create_time': self.create_time,
            'update_time': self.update_time,
            'company': self.company,
            'contact_name': self.contact_name,
            'phone': self.phone,
            'cellphone': self.cellphone,
            'email': self.email,
            'city': self.city,
            'department': self.department,
            'current_softpymes': self.current_softpymes,
            'status': self.status,
        }

    @staticmethod
    def get_referrals():
        """
            allow obtain all colors
            :return an array of color objects in JSON format
        """
        # data=[color.export_data() for color in session.query(Color).order_by(Color.code).all()]
        referrals = data=[referral.export_data() for referral in session.query(Referral).all()]
        response = jsonify(referrals)

        return response


        # color = session.query(Color).get(color_id)
        # if color is None:
        #     response = jsonify({'error': "Not Found", 'message': 'Not Found'})
        #     response.status_code = 404
        #     return response
        # color = color.export_data()
        # response = jsonify(color)
        # return response
        #color = jsonify(data=[color.export_data() for color in session.query(Color).order_by(Color.code).all()])
        #return color