# -*- coding: utf-8 -*-

from ... import Base, session
from ...utils.image_converter import ImagesConverter
from sqlalchemy import String, Integer, Column, DateTime, VARBINARY, or_, ForeignKey
from sqlalchemy.orm import relationship, joinedload
from sqlalchemy.dialects.mysql import TINYINT
from werkzeug.security import generate_password_hash, check_password_hash
from datetime import datetime, timedelta
from ...exceptions import ValidationError, InternalServerError
from jose import jwt, JWSError, ExpiredSignatureError
import os
from flask import g, abort

class User(Base):

    __tablename__ = 'users'

    user_id = Column(Integer, primary_key=True)
    username = Column(String(50))
    email = Column(String(255))
    password = Column(String(2000), default=None, nullable=True)
    create_time = Column(DateTime, default=datetime.now())
    approved = Column(TINYINT, default=None, nullable=False)
    status = Column(TINYINT, default=None, nullable=False)


    def generate_auth_token(self, user):
        """
        Allow generate a token by un new user
        :param user: full user data
        :return: string token
        """
        payload = {
            "sub": str(user.userId),
            "iat": datetime.utcnow(),
            "exp": datetime.utcnow() + timedelta(days=1),
            "name": str(user.full_name()),
            "last_branch_id": str(user.lastBranchId)
        }
        token = jwt.encode(payload, os.environ.get("SECRET_KEY"), algorithm="HS256")
        return token

    @staticmethod
    def verify_auth_token(token):
        """
        Allow validate a user according a yours token
        :param token: token to validate
        :raise: ValidationError an error occurs when no auth token faile
        :return: token encode or 401 unauthorized
        """
        if token is None or token == "":
            return None
        try:
            jw = jwt.decode(token, os.environ.get("SECRET_KEY"), algorithms=["HS256"])
            g.user = jw
            return jw
        except JWSError as e:
            raise ValidationError(e)
        except ExpiredSignatureError as e:
            return None
        except Exception as e:
            return None


    @staticmethod
    def find_user(username, password):
        """
        Allow find an user from username and password
        :param username: username by user to search
        :param password:  password hash by user to search
        :return: user object or None otherwise
        """
        try:
            user = session.query(User).filter(User.username.ilike('%'+username+'%')).first()
            if user is None:
                return None
            if user.verify_password(password):
                return user
            else:
                return None
        except Exception as e:
            print(e)
            session.rollback()
            raise InternalServerError(e)
