# -*- coding: utf-8 -*-



from ... import Base, session
from sqlalchemy import String, Integer, Column, DateTime, VARBINARY, or_, ForeignKey
from sqlalchemy.orm import relationship, joinedload
from sqlalchemy.dialects.mysql import TINYINT
from werkzeug.security import generate_password_hash, check_password_hash
from datetime import datetime, timedelta
from ...exceptions import ValidationError, InternalServerError
from jose import jwt, JWSError, ExpiredSignatureError
import os
from flask import g, abort

class Profile(Base):

    __tablename__ = 'profiles'

    profile_id =Column(Integer, primary_key=True)
    identification_number = Column(String(45))
    create_time = Column(DateTime, default=datetime.now())
    update_time = Column(DateTime, default=datetime.now())
    cellphone = Column(String(45))
    sex = Column(String(10))
    company_name = Column(String(255))
    city = Column(String(45))
    department = Column(String(45))
    address = Column(String(45))
    names = Column(String(45))
    last_name = Column(String(255))
    photo = Column(String(255))
    alternative_email = Column(String(45))

    user_id = Column(Integer)
    #user_id = Column(Integer, ForeignKey('user.user_id'))
    #user = relationship('User', foreign_keys=[user_id])

