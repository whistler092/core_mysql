# -*- coding: utf-8 -*-
#!/usr/bin/env python
#########################################################
# Referential module
# All credits by SoftPymes Plus
# 
# Date: 21-07-2016
#########################################################
__author__ = "SoftPymes"
__credits__ = [""]
__version__ = "1.0.1"



class ValidationError(ValueError):
	"""
	"""
	pass


class IntegrityError(ValueError):
	"""
	"""
	pass


class InternalServerError(ValueError):
    """
    """
    pass
