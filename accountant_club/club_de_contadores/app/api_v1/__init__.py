# -*- coding: utf-8 -*-
#########################################################
# All rights by SoftPymes Plus
#
# API_v1 Module
#########################################################
__author__ = "SoftPymes"
__credits__ = [""]
__version__ = "1.0.1"



from flask import Blueprint
from ..auth import auth_token

api = Blueprint('api', __name__)


@api.before_request
@auth_token.login_required
def before_request():
    """<b>Description:</b>All routes in this blueprint require authentication"""
    pass


@api.after_request
def after_request(response):
    """
    <b>Description:</b> that allows restricted or not resources on a web page to be
     requested from another domain outside the domain from which the resource originated.
     SoftPymes Plus is (*)
    """
    response.headers.add('Access-Control-Allow-Origin', '*')
    response.headers.add('Access-Control-Allow-Headers', 'Content-Type,Authorization')
    response.headers.add('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE')
    return response


## Imports by APP -- api_v1
# from .referential import asset_groups
from .referential import referrals