# -*- coding: utf-8 -*-
#!/usr/bin/env python
#########################################################
# Referential module
# All credits by SoftPymes Plus
#
# Date: 21-07-2016
#########################################################
__author__ = "SoftPymes"
__credits__ = [""]
__version__ = "1.0.1"



from flask import request, jsonify, Response
import json
from .. import api
from ...models import Referral
#from ...decorators import json, authorize
from ... import session


@api.route('/referrals/', methods=['GET'])
def get_referrals():
    """
    # /colors/
    <b>Methods:</b> GET <br>
    <b>Arguments:</b> None <br>
    <b>Description:</b> Return all colors<br/>
    <b>Return:</b> json format
    """
    response = Referral.get_referrals()
    return response
